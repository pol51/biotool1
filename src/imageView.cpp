#include "imageView.h"

#include <QtGui/QMouseEvent>

ImageView::ImageView(QWidget *parent) :
  QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DoubleBuffer), parent),
  dataCtrl(new DataCtrl(this))
{
  connect(&refreshTimer, SIGNAL(timeout()), SLOT(update()));
  refreshTimer.start(20);
}

ImageView::~ImageView()
{
  if (imageTexId) deleteTexture(imageTexId);
  delete dataCtrl;
}

void ImageView::mousePressEvent(QMouseEvent *event)
{
  if (event->button() == Qt::LeftButton)
  {
    const DataCtrl::EMode CurrentMode = dataCtrl->currentMode();
    onMoveDecal = (event->type() == QEvent::MouseButtonPress && CurrentMode == DataCtrl::eModeView);
    lastMousePos = event->pos();

    if ((1<<CurrentMode) & ((1<<DataCtrl::eModeEdit) | (1<<DataCtrl::eModeDefineCentroid)))
    {
      const float factor = qMin(width(), height());
      float x(zoom / 10.);
      float y(x);
      if (width() > height())
        x *= ((float)width() / (float)height());
      else
        y *= ((float)height() / (float)width());
      QPointF Point((event->x() / factor * (zoom<<1) - xDecal) /10. - x,
                    (event->y() / factor * (zoom<<1) - yDecal) /10. - y);

      dataCtrl->addPoint(Point);
    }
  }

  setFocus();

  QGLWidget::mousePressEvent(event);
}

void ImageView::mouseReleaseEvent(QMouseEvent *event)
{
  if (event->button() == Qt::LeftButton)
    onMoveDecal = false;

  QGLWidget::mousePressEvent(event);
}

void ImageView::mouseMoveEvent(QMouseEvent *event)
{
  float factor = qMin(width(), height());
  if (onMoveDecal && event->type())
  {
    xDecal += (event->x() - lastMousePos.x()) / factor * (zoom<<1);
    yDecal += (event->y() - lastMousePos.y()) / factor * (zoom<<1);
  }
  lastMousePos = event->pos();

  if (!onMoveDecal && !(event->buttons() & Qt::LeftButton))
    QGLWidget::mouseMoveEvent(event);
}

void ImageView::wheelEvent(QWheelEvent *event)
{
  zoom += event->delta() / 120;
  if (zoom > 11)  zoom = 11;
  if (zoom < 1)   zoom = 1;
  resizeGL(width(), height());

  QGLWidget::wheelEvent(event);
}

void ImageView::keyPressEvent(QKeyEvent *event)
{
  switch (event->key())
  {
    case Qt::Key_Backspace:
      if (event->modifiers() & Qt::ControlModifier)
        dataCtrl->removeLastForm();
      else
        dataCtrl->removeLastPoint();
      break;
    case Qt::Key_Space:
      dataCtrl->finalizeForm();
      break;
    default:
      break;
  }

  QGLWidget::keyPressEvent(event);
}

void ImageView::doChangeImage(const QImage &image)
{
  if (imageTexId) deleteTexture(imageTexId);
  GLfloat RatioWidthPerHeght = (image.width() / (float)image.height());
  quadVertices[0] = quadVertices[3] = -RatioWidthPerHeght;
  quadVertices[6] = quadVertices[9] =  RatioWidthPerHeght;

  imageTexId = bindTexture(image);
  glBindTexture(GL_TEXTURE_2D, imageTexId);
}

void ImageView::doCloseImage()
{
  if (imageTexId) deleteTexture(imageTexId);
  quadVertices[0] = quadVertices[3] = -1.f;
  quadVertices[6] = quadVertices[9] =  1.f;

  imageTexId = 0;
}

void ImageView::initializeGL()
{
  QGLWidget::initializeGL();

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  glEnable(GL_TEXTURE_2D);

  glVertexPointer(3, GL_FLOAT, 0, quadVertices);
  glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
}

void ImageView::paintGL()
{
  QGLWidget::paintGL();

  glLoadIdentity();
  glTranslatef(xDecal / 10., yDecal / 10., 0);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  glColor4f(1.f, 1.f, 1.f, 1.f);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, quadIndexes);

  dataCtrl->draw();

  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
}

void ImageView::resizeGL(int width, int height)
{
  QGLWidget::resizeGL(width, height);

  glViewport(0, 0, width, height);

  float x(zoom / 10.);
  float y(x);

  if (width > height)
    x *= ((float)width / (float)height);
  else
    y *= ((float)height / (float)width);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glClearColor(0.f, 0.f, 0.f, 0.f);
  glOrtho(-x, +x, +y, -y, -100.0, 100.0);
  glMatrixMode(GL_MODELVIEW);
}
