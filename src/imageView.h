#ifndef __IMAGEVIEW_H__
#define __IMAGEVIEW_H__

#include <QtOpenGL/QGLWidget>

#include <QtCore/QTimer>

#include "dataCtrl.h"

class ImageView : public QGLWidget
{
  Q_OBJECT

  public:
    ImageView(QWidget *parent = NULL);
    ~ImageView();

    void changeMode(DataCtrl::EMode mode) { dataCtrl->setCurrentMode(mode); onMoveDecal = false; }

    DataCtrl& data() { return *dataCtrl; }
    const DataCtrl& data() const { return *dataCtrl; }

  public slots:
    void doZoomIn()     { if (zoom > 1) --zoom; }
    void doZoomOut()    { if (zoom < 11) ++zoom; }
    void doResetView()  { zoom = 10; xDecal = yDecal = 0.; resizeGL(width(), height()); }

    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void wheelEvent(QWheelEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);

  protected slots:
    void doChangeImage(const QImage &image);
    void doCloseImage();

  protected:
    virtual void initializeGL();
    virtual void paintGL();
    virtual void resizeGL(int w, int h);

  protected:
    bool onMoveDecal = false;
    int zoom = 10;
    DataCtrl *dataCtrl = NULL;
    GLuint imageTexId = 0;
    GLfloat xDecal = 0.f, yDecal = 0.f;
    QTimer refreshTimer;
    QPoint lastMousePos;

    GLfloat quadVertices[12] = {
      -1.f, -1.f, 0.f,
      -1.f,  1.f, 0.f,
       1.f,  1.f, 0.f,
       1.f, -1.f, 0.f
    };

    const GLubyte quadIndexes[6] = {
      0,1,2,
      0,2,3
    };

    const GLfloat texCoords[8] = {
      0.f, 1.f,
      0.f, 0.f,
      1.f, 0.f,
      1.f, 1.f
    };
};

#endif
