#ifndef __SETTINGS_H__
#define __SETTINGS_H__

#include <QtGui/QColor>

class Settings
{
  public:
    static void Load();
    static void Save();
};

#endif
